#!/bin/bash
#Officium - Reinstallation Script

#Details
project_link="https://gitlab.com/thaiswnielsen/itt2_project_officium/-/archive/master/itt2_project_officium-master.zip"
gitrepourl="git@gitlab.com:thaiswnielsen/itt2_project_officium.git"
project_name="itt2_project_officium"
flask_api_path="/Flask_api/menu.py"
Allowed_users=(karthieuni thaiswnielsen Henry1234)

#Networking settings
Interface="eth0"
ip="10.10.1.2/24"
Router_ip="10.10.1.1"
DNS_server="8.8.8.8"

#Add network config for static IP
echo "[ \e[32m* ] Configuring static IP"
echo "" >> /etc/dhcpcd.conf
echo "interface $Interface" >> /etc/dhcpcd.conf
echo "static ip_address=$Ip" >> /etc/dhcpcd.conf
echo "static routers=$Router_ip" >> /etc/dhcpcd.conf
echo "static domain_name_servers=$DNS_server" >> /etc/dhcpcd.conf

#Enable SSH
echo "[ \e[32m* ] Enabling SSH."
sudo systemctl enable ssh
sudo systemctl start ssh

#Enable Serial
echo "[ \e[32m* ] Enabling serial ports"
echo "" >> /boot/config.txt
echo "enable_uart=1" >> /boot/config.txt

#Add user's SSH-keys
echo "[ \e[32m* ] Setting up SSH"
sudo mkdir /home/pi/.ssh
touch /home/pi/.ssh/authorized_keys
echo "" > /home/pi/.ssh/authorized_keys

for user in ${Allowed_users[*]}
do
    echo item: Adding User: $user
    curl https://gitlab.com/$user.keys >> /home/pi/.ssh/authorized_keys
done

#Install packages
sudo echo "Updating packages"
sudo apt-get update && sudo apt-get upgrade -y
echo "[ \e[32m* ] Install python3-pip"
sudo apt-get -y -q install python3-pip
echo "[ \e[32m* ] Install unzip"
sudo apt-get install unzip
echo "[ \e[32m* ] Install pyserial"
sudo pip3 install pyserial -q
echo "[ \e[32m* ] Install flask"
pip3 install flask -q
pip3 install flask_restful -q
pip3 install flask_cors -q

#Enabling UART
sudo echo "Enabling UART"
sudo echo "enable_uart=1" | sudo tee -a /boot/config.txt >> /dev/null
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service
sudo sed -e s/console=serial0,115200//g -i.backup /boot/cmdline.txt

#Get zip-file
echo "[ \e[32m* ] Downloading projekct"
wget -O $project_name.zip $project_link
#unzip file
unzip -o $project_name.zip

#Flask auto-run
echo "[ \e[32m* ] Setup auto-run Flask"
echo "[Unit]" > /lib/systemd/system/flask_api.service
echo "Description=Flask api service" >> /lib/systemd/system/flask_api.service
echo "[Service]" >> /lib/systemd/system/flask_api.service
echo "Type=static" >> /lib/systemd/system/flask_api.service
echo "ExecStart=/usr/bin/python3 /home/pi/"$project_name$flask_api_path >> /lib/systemd/system/flask_api.service
echo "[Install]" >> /lib/systemd/system/flask_api.service
echo "WantedBy=boot-complete.target" >> /lib/systemd/system/flask_api.service

#Clone repository
sudo echo "Cloning git repository"
sudo git clone $gitrepourl

sudo chmod 644 /lib/systemd/system/flask_api.service

sudo systemctl daemon-reload
sudo systemctl enable flask_api.service

#Ping test (internet and router)
sudo ping $router -c 5
sudo wget --spider google.com
if [ $? -eq 0 ]; then
	conn=1
else
	conn=0
fi

if [ $conn -ne 1 ]; then

sudo echo "Done\n\n"

sudo reboot
