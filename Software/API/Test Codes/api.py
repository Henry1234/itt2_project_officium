from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
from atmega import *

app = Flask(__name__)
CORS(app, resources=r'/*')  # to allow external resources to fetch data
api = Api(app)


@api.resource("/")
class url_index(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = read_temp(ser)
        return returnMessage


api.resource("/temp")
class url_gpio(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
           returnMessage = read_temp(ser)
        return returnMessage


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
