usage
========
* atmega5.py - https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Software/API/atmega5.py
* api2.py - https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Software/API/api2.py

atmega5.py is a dictronary which stores all the commands which the API can use to communicate with the dashboard

atmega5.py is added to the api2.py

command to run api2.py
```
python3 api2.py
```
once the program is running, type led, temp, btn at the end of the API:

* http://10.217.19.215:5001/led
* http://10.217.19.215:5001/temp
* http://10.217.19.215:5001/btn

to access the dashboard: http://10.217.16.42/

test usage
========

start the virtual environment

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

run the service

```
python restex.py
```

access it by opening the file `index.html` and click the buttons

When done, you can exit the virtual environment
```
deactivate
```
