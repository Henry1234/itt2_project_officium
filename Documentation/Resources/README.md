Find the latest resources by clicking on this link: [https://eal-itt.gitlab.io/19s-itt2-project/](https://eal-itt.gitlab.io/19s-itt2-project/)

The resource folder contains the following files:

* Exercises (PDF)
* Exercises (MD)
* Weekly Plans (PDF)
* Weekly Plans (MD)
* lecture_plan.pdf
* project plan for students (PDF)
* project plan for students (MD)



