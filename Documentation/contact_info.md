# Karthiebhan Mahendran

### Discord
- Karthie
### School e-mail
- kart0050@edu.eal.dk
### Gitlab
- @karthieuni
### Riot
- @karthie1997:matrix.org

# Thais W. Nielsen

### Discord
- Dalbert
### School e-mail
- thai0051@edu.eal.dk
### Gitlab
- @thaiswnielsen
### Riot
- @dalbertnano:matrix.org

# Lauris Henrijs Valdovskis

### Discord
- 
### School e-mail
- laur177n@edu.eal.dk
### Gitlab
- @Henry1234
### Riot
- @henrijs_valdovskis:matrix.org

# Nikolaj Simonsen
### E-mail
- nisi@ucl.dk
### Gitlab
- @npes
### Riot
- @npesdk:matrix.org

# Morten Bo Nielsen
### E-mail
- mon@ucl.dk
### Gitlab
- @moozer
### Riot
- @moozer:matrix.org