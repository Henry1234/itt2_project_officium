# Project Plan

## Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. 
This project will cover the first part of the project, and will match topics that are taught in parallel classes. 
In our project case we have been given the task of creating a system which can monitor the different kinds of environment variables within a standard server room, so that we can faster response to inbound emergencies and situations alike.

## Purpose

The main goal is to have a system where data from the different sensors is send to a cloud server 
which is handling the data by collecting, presenting, and responding back to which different commands to the system.
The system have different sensors like temperature sensor, magnetic switch sensor, humidity sensor, and if possible a live camera feed which all goes through an Raspberry Pi setup
connected with a microprocessor. All of this setup will be connected to a juniper router which is connected to some webservers which themself is displaying the different values of the 
sensors, then through Restful API we can control different options of this system on a webclient.

For simplicity, the goals stated will revolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

## Goals

<h3> High Level Diagram <h3>

![Diagram](../Design%20Files/Topology/diagram.png)

<h3>Setup<h3>

![Setup](../Software/Codes/arduino_setup.png)

The overall system that is going to be build looks as follows

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the serial connection to the raspberry pi.
* Raspberry Pi: Minimal linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* Webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.

Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Documentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.

## Schedule

The working days are **Monday** and **Tuesday**.

* Link to the lecture plan : [https://eal-itt.gitlab.io/19s-itt2-project/](https://eal-itt.gitlab.io/19s-itt2-project/)

### Monday schedule
* 8:15 - 8:30 Group meeting
* 8:30 - 11:30 Lecture
* Lunch 
* 12:15 - 13:00 Claim issues
* 13:00 - 15:30 Issues work time

### Tuesday schedule
* 8:15 - 8:30 Group meeting
* 8:30 - 11:30 Issues work time/ lecture & group meetings 
* Lunch
* 12:15 - 14:30 Group discussion
* 14:30 - 15:30 Week summary and planning

## Organization

<table align="center"><tr><td align="center" width="9999">

Name | Email
------|------
Karthiebhan Mahendran|kart0050@edu.eal.dk
Thais W. Nielsen|thai0051@edu.eal.dk
Lauris Henrijs Valdovskis|laur177n@edu.eal.dk

</td></tr></table>

## Budget and resources

* Arduino Uno - ATMega328p - 69kr
* TMP36GT9Z - temperature sensor - 0kr
* Wires for the GPIOs pins - 0kr
* USB printer cable to power - Arduino - 0kr
* Power supply - Raspberry Pi - 0kr
* Micro SD Card - 120kr
* Level shifter - 0kr
* Push button - 0kr
* Raspberry Pi - 319kr
* Router - SRX-240 - 0kr
* Ethernet cable (RJ45 - CAT5E or 6) - 0kr
* Console wire for SRX-420 - 0kr
* Veroboard & pins - 0kr
* RGB LED (Status LED) - 0kr
* Datahousing - 0kr

Total price : 518 kr

Link to the [Team Management](../Documentation/Team%20Management)

## Risk assessment

<h3>Lack of documentation<h3>

Solution to the problem: 
* Assign a stakeholder to be responsible for documentation (assign tasks)

<h3>Lack of knowledge<h3>

Solution to the problem:
* Seek knowledge by asking for help from teammates and/or classmates
* Be honest with teammates, don't hide your progress
        

<h3>Lack of motivation<h3>

Solution to the problem:
* Don't hide it from teammates, have a team talk and find a solution

<h3>Bad planning<h3>
  
Solution to the problem:
*  Keep group meetings relevant
*  Solve all the problems as soon as possible

## Stakeholders

 This project is expected to have more stakeholders then other projects cases,  because this project can be a risk to many more elements due to the fact that it's going to be connected with a form of public infrastructure
and in turn could be a risk to the infrastructure itself.

Possible stakeholders related to this project:

    • Employees
    Every member and their families of the project is some of the stakeholders of the project because if the project
    experience any kind of negative impact it could also affect the employees, and their income stream.
    
    • Customers
    The customers of this product / service also have some stake connected with this project in the way that if they
    don't get theirs money worth, they are losing money on a poor quality product.
    
    • Investors
    If the project has any form of investors we can expect that they themselves are expecting a return profit on this
    project, so if the project fails the investors are losing money and or resources.
    
    • Suppliers
    The suppliers of different resources mostly common matarials to the project is excpting some kind of a lifetime for
    the project so they can make an estimate of how long, and how much money they can expect from you as an income source,
    if the project fails the income stream stops.
    
    • Communities
    One of the stakeholders connected to the project is the community, because any major decisions can affect the community
    as a whole this makes the community a passive stakeholder of the project.

    • Government
    In the project we are working with a public institution, which means that this project is affecting a banche of the 
    goverment, and in turn could improve the given institution or worsen it. 

A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

## Communication

Communication is essential in project and needs to be carefully focused on for the project to be successful. 

*  Scheduled stakeholder meetings on gitlab every week
*  Facebook group for regular messaging
*  Project status reports
*  Individual tasks for every stakeholder on trello and gitlab
*  Gitlab issues has to be approved by everyone in the group before they're closed (commenting and linking the documents). 

## Perspectives

Gitlab pages:

* Project: https://thaiswnielsen.gitlab.io/itt2_project_officium/
* Project (Network Configurations): https://gitlab.com/karthieuni/ITT2_week7_Officium_SII_FAN
* karthie.gitlab.io: https://karthieuni.gitlab.io/karthie.gitlab.io/
* henry.gitlab.io: https://henry1234.gitlab.io/henry.gitlab.io/
* thais.gitlab.io: https://thaiswnielsen.gitlab.io/thaiswnielsen.gitlab.io

### Description
The perspectives section contains information on who and how will benefit in relation to the project.

### Group members  
The group members will learn how to cooperate within the group. The communication skills are very important in real life working environment. 
The GitLab platform will be used as a communication platform for project management.
The team will acquire knowledge working as a team and using GitLab to manage the project. 
The team will use their knowledge from the other classes (programming, electronics, and network).

### Lecturers  
The lecturers will be able to benefit from the project also. The knowledge acquired by students and vice versa can be shared. The lecturers will receive feedback, which will enable them to improve in the future.

### Employers  
The employers will be able to employ the students, who have possesed the knowledge in programming, electronics and networking. 

### Users  
The users can take advantage of the system produced within the project with application to different areas of everyday life.

### Institution  
The UCL University College Denmark will be recognised as a higher educational institution which is offering quality education and help the students as much as possible to get decent education and fulfilling job in the future.

## Evaluation

### Group Evaluation

* Evaluate teamwork and communication
* Evaluate process

### Process Evaluation

* Control the LED on/off button from the dashboard (webserver)
* When hold the button, the result value is 1, if the button is released the value is 0
* Status field shows values with 1 or 0 - dashboard (webserver)
* Send and read temperature readings and can display it on a graph using the dashboard (webserver)
* Fix errors
* Let other developers test the product

### End Evaluation

* Make the users test the product
* Evaluate the project

## References

1. “Schematic : ATMega328 Sensor Sample / Schematics Orcad.” GitLab. Accessed January 29, 2019. https://gitlab.com/atmega328-sensor-sample/schematics-orcad.
2. “TMP36GT9Z datasheet : 0900766b80ae1656.Pdf.” Accessed January 29, 2019. https://docs-emea.rs-online.com/webdocs/0ae1/0900766b80ae1656.pdf.
3. “TMP36GT9Z testing : TMP36 Temperature Sensor | Adafruit Learning System.” Accessed January 29, 2019. https://learn.adafruit.com/tmp36-temperature-sensor/testing-a-temp-sensor.
4. “Fritzing : Fritzing Download.” Accessed January 29, 2019. http://fritzing.org/download/.
