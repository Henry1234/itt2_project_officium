[Gitlab SSH](https://docs.gitlab.com/ee/ssh/)

#To enable SSH:
sudo raspi-config

#To set a static IP address:
sudo nano /etc/dhcpcd.conf

1. interface eth0
2. static ip_address=10.10.***1.2***/24
3. static routers=10.10.***1.1***
4. static domain_name_servers=8.8.8.8

#To assign the port for the RPi SSH connection:

1. sudo nano /etc/ssh/sshd_config
2. port 1800.***1***

#Mount USB
mkdir Media
chmod 777 Media/
sudo blkid
sudo mount -o uid=pi,gid=pi /dev/sda1 /home/pi/Media

sudo chmod 777 /home/pi/Media/.ssh/
sudo mv id_rsa.pub /etc/ssh/ssh_rsa_key.pub

#Commands
ssh pi@10.10.1.2 -p 18001
ssh pi@10.217.19.215 -p 18001

#Locate SSH Public Key
cd /home/pi/.ssh/id_rsa.pub
