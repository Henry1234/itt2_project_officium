Group: Officium

--------------------------------------------

Members:

* Karthiebhan Mahendran karthie1997
* Thais W. Nielsen dalbertnano
* Lauris Henrijs Valdovskis henrijs_valdovskis

Juniper router:

* Management SSH: 10.217.19.215:22
* External ip: 10.217.19.215

Raspberry:

* SSH access: 10.217.19.215:18001
* REST API access: [http://10.217.19.215:5001](http://10.217.19.211:5001)

Group web server:

* SSH access: 10.217.16.42:22
* REST API access: [http://10.217.16.42:80](http://10.217.16.42:80)
