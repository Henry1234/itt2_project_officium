Status Protocol - LED Blinking
-------------------

| Behaviour      | Meaning/Problem           |         Solution          |
| :-----------:  |:------------------------: | :-----------------------: |
| Blinking red   | No connection to sensor   | Call support              |
| Constant blue  | Manual mode/OK            | Do nothing                |
| Constant green | Auto mode/OK              | Do nothing                |
| Constant white | No connection to internet | Call support              |